##Multiple uses
 variable "project_name" {
     type = string
 }

variable "backend_facts" {
    default = [
        { ip = "1.2.3.4/32", description = "cookit-backend-0" },
        { ip = "5.6.7.8/32", description = "cookit-backend-1" }
    ]
}
