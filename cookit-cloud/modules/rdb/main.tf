terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.1.0"
    }
  }
  required_version = ">= 0.15"
}

resource "scaleway_rdb_instance" "rdb_main" {
  name           = "test-rdb"
  node_type      = "db-dev-s"
  engine         = "PostgreSQL-12"
  is_ha_cluster  = false
  disable_backup = true
  user_name      = "my_initial_user"
  password       = "thiZ_is_v&ry_s3cret"
}

resource "scaleway_rdb_acl" "rdb_acls" {
  instance_id = scaleway_rdb_instance.rdb_main.id

  dynamic "acl_rules" {
    for_each = var.backend_facts
    iterator = acl
    content {
      ip                 = acl.value.ip
      description        = acl.value.description
    }
  }
}

resource "scaleway_rdb_database" "rdb_db" {
  instance_id    = scaleway_rdb_instance.rdb_main.id
  name           = var.project_name
}
