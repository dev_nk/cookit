terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.1.0"
    }
  }
  required_version = ">= 0.15"
}

resource "scaleway_instance_volume" "data" {
  count = var.volume_size == 0 ? 0 : var.nbr_of_node

  name = "${var.project_name}-vol-${count.index}"
  size_in_gb = var.volume_size
  type = var.volume_type
}

resource "scaleway_instance_server" "node" {
  type  = var.type
  image = var.image
  name = "${var.project_name}-compute-${count.index}"

  count = var.nbr_of_node
  tags = [ "cookit-${count.index}" ]

  enable_dynamic_ip = true

  additional_volume_ids = var.volume_size == 0 ? [] : [scaleway_instance_volume.data[count.index].id]

  root_volume {
    size_in_gb = 20
  }

  user_data = {
    cloud-init = file("${path.module}/files/cloud-init.yml")
  }

  security_group_id = var.security_group_id
}

resource "local_file" "ansible_inventory" {
  # Few comments :
  #  * Keys map contains <name>.<environment>, we need to concat this with dns suffix to allow Ansible to resolve FQDN
  #    We already ask for a dns zone, so we will just use a replace with a regex to remove the last '.' character
  #  * We map according to Ansible groups that are defined within the playbook
  #  * We export this inventory using INI format in the directory ansible_inventory/<environment>
  content = templatefile("${path.module}/../../templates/ansible_inventory.tpl", {
    all_ips = join(",", scaleway_instance_server.node.*.public_ip)
  })

  filename = "../../cookit-provision/inventories/dev/hosts"
  file_permission = "0600"
}
