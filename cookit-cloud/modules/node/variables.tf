##Multiple uses
 variable "project_name" {
     type = string
 }

 # Compute instance
variable "image" {
    type    = string
    default = "debian_buster"
}

variable "nbr_of_node" {
    type = number
    default = 1
}

variable "type" {
    type = string
    default = "DEV1-S"
}

# Additional volume
variable "volume_size" {
    type = number
    default = 0
}

variable "volume_type" {
    type = string
    default = "l_ssd"
}

# Network
variable "security_group_id" {
    type = string
}
