output "servers_ip" {
  value = scaleway_instance_server.node.*.public_ip
}
