terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.1.0"
    }
  }
  required_version = ">= 0.15"
}

resource "scaleway_instance_security_group" "fw" {
  name = "fw-${var.project_name}"

  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"

  inbound_rule {
    action = "accept"
    port   = "22"
  }

  inbound_rule {
    action = "accept"
    port   = "80"
    ip = var.test_lb_front_ip
  }

  inbound_rule {
    action = "accept"
    port   = "443"
    ip = var.test_lb_front_ip
  }
}
