terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.1.0"
    }
  }
  required_version = ">= 0.15"
}

resource "scaleway_lb_ip" "ip" {
}

resource "scaleway_lb" "base" {
  ip_id  = scaleway_lb_ip.ip.id
  type   = "LB-S"
  name   = "lb-${var.project_name}" 
}

resource "scaleway_lb_frontend" "frontend01" {
  lb_id        = scaleway_lb.base.id
  backend_id   = scaleway_lb_backend.back.id
  name         = "lb-${var.project_name}-front"
  inbound_port = "80"
}

resource "scaleway_lb_backend" "back" {
  lb_id            = scaleway_lb.base.id
  name             = "lb-${var.project_name}-back"
  forward_protocol = "http"
  forward_port     = "80"

  server_ips       = var.backends_ip
  # TODO: health check
}
