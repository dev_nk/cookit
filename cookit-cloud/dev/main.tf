terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.1.0"
    }
  }
  required_version = ">= 0.15"

  # gitlab-ci
  #backend "http" {
  #}
}

# TODO: var pour l'env (ex: cookit-dev1)
# TODO: ssl (2 ways ?) avec la base

provider "scaleway" {
  zone            = "fr-par-1"
  region          = "fr-par"
  project_id      = "6ad5d37c-7395-4e9d-b090-19e73bb5e41b"
  access_key      = var.scw_access_key
  secret_key      = var.scw_secret_key
}

module "security_group" {
  source = "../modules/securityGroup"

  project_name = "cookit"
  test_lb_front_ip = module.lb.frontend_ip
}

module "node" {
  source = "../modules/node"

  project_name = "cookit"
  security_group_id  = module.security_group.fw_id
  nbr_of_node = 1
  # volume_size = 50
}

# For load, but (even more important) for redundancy / high availability
module "lb" {
  source = "../modules/lb"

  project_name = "cookit"
  backends_ip = module.node.servers_ip
}
