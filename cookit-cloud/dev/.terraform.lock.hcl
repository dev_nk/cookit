# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.1.0"
  constraints = "2.1.0"
  hashes = [
    "h1:IfyjOGvS3B9fsA1s4XSr0ajVlufSMq/bbqAX8DODdcY=",
    "zh:0de647cba4e5a5973984a0e223aa5b6dc06022a0ed9ddc491d4cc1543d4516f0",
    "zh:1400a65eaacab234db4734966e79d8891def0e5b9a10f7bad34efcfd6b42ac78",
    "zh:1441133bdae2cbaf34203d831a49c535177533023281e1f884bc699adcdcd94d",
    "zh:1d3efd87c78f6ab27e4df189510eb84e0b9f1f4eb0d8c0b983e6795824ceb6b3",
    "zh:30db64a0dcc43784062ae09dc2b2741fba657577804e7f130aeca2bed8ee6f8c",
    "zh:359d420de3ca104c73acd6a0d84af445ee9d6a2759b7131a4f378e786f8cc027",
    "zh:6f6a1781251881309289f4ce2aa4289a8715f33ac50603dcb9beae047450892f",
    "zh:731de7d68e140f21004506a3afdf7c1af0e285c986e98c53ebde113deb5d4b94",
    "zh:90f4fcdce03b46da0b1da88888d9998a61f2f5f50566f88693bc98bdff9117ff",
    "zh:a88b770b34cddf506c0c38c98dd124dc7388c6c1baafe546f60f09ba6a1e5691",
    "zh:ae7f8250c07e38e86e15bb4f27de57756d49914ee0146298dd0b6f140380a62c",
    "zh:cc3f1ad2cb22e50a578cdda1e02bf94f8ce85cebf691263e4b7da8e864e3195d",
    "zh:cfb66ebd80a07f160da683d05c6ce03faa2c3218998eaa2df7b1e745026bd4b8",
  ]
}
