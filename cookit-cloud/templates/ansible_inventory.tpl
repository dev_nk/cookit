[all]
%{ for idx, ip in split(",", all_ips) ~}
node${idx} ansible_host=${ip}
%{ endfor ~}
