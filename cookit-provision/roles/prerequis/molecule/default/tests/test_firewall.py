import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_iptables_rules_exists(host):
    with host.sudo():
        assert len(host.iptables.rules()) > 0


def test_iptables_accept_ssh(host):
    wanted_rule = '-A INPUT -p tcp -m tcp --dport 22 -j ACCEPT'
    with host.sudo():
        assert wanted_rule in host.iptables.rules()


def test_iptables_ends_with_reject_all(host):
    with host.sudo():
        assert host.iptables.rules()[-1] == '-A INPUT -j DROP'
