import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_profilecolor(host):
    profile_file = host.file('/etc/profile.d/profilecolor.sh')
    assert profile_file.exists
    assert profile_file.user == 'root'
    assert profile_file.group == 'root'
    assert profile_file.mode == 0o744


def test_autologout(host):
    autologout_file = host.file('/etc/profile.d/autologout.sh')
    assert autologout_file.exists
    assert autologout_file.user == 'root'
    assert autologout_file.group == 'root'
    assert autologout_file.mode == 0o744
